//  Created by Karen Lusinyan on 10/23/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

#import "CDUUtil.h"
#import "CoreDataUtilsConst.h"

@implementation CDUUtil

+ (void)load
{
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        NSLog(@"[CoreDataUtils] Version %@", VERSION);
    });
}

+ (NSDictionary *)dictionaryFromManagedObject:(NSManagedObject *)managedObject
                             includeNilValues:(BOOL)includeNilValues
{
    NSArray *attributes = [NSArray arrayWithArray:[[[managedObject entity] attributesByName] allKeys]];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    for (id attribute in attributes) {
        id value = [managedObject valueForKey:attribute];
        if (includeNilValues) {
            [dict setObject:(value != nil) ? value : [NSNull null] forKey:attribute];
        }
        else if (value != nil) {
            [dict setObject:value forKey:attribute];
        }
    }
    return dict;
}

@end
