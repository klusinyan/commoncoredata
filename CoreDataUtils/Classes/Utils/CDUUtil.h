//  Created by Karen Lusinyan on 10/23/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

@interface CDUUtil : NSObject

+ (NSDictionary *)dictionaryFromManagedObject:(NSManagedObject *)managedObject
                             includeNilValues:(BOOL)includeNilValues;

@end
