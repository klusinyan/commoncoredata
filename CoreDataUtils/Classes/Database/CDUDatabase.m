//  Created by Karen Lusinyan on 9/3/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

#import "CDUDatabase.h"
#import "CDUUtil.h"
#import "NSManagedObjectContext+CDU.h"

static NSMutableDictionary *CDUDatabaseStack = nil;
//static NSManagedObjectContext *persistentStoreManagedObjectContext = nil;
//static CDUDatabase *sharedInstance = nil;

@interface CDUDatabase ()

@property (readwrite, nonatomic, strong) NSManagedObjectContext *context;

@end

@implementation CDUDatabase

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#if !__has_feature(objc_arc)
    [_context release];
    [super dealloc];
#endif
}

//log the current context concurrency type
- (NSString *)formatTypeToString:(NSManagedObjectContextConcurrencyType)formatType
{
    NSString *result = nil;
    
    switch(formatType) {
        case NSConfinementConcurrencyType:
            result = @"NSConfinementConcurrencyType";
            break;
        case NSPrivateQueueConcurrencyType:
            result = @"NSPrivateQueueConcurrencyType";
            break;
        case NSMainQueueConcurrencyType:
            result = @"NSMainQueueConcurrencyType";
            break;
        default:
            [NSException raise:NSGenericException format:@"Unexpected FormatType."];
            break;
    }
    return result;
}

//the logic prior to v1.4.0 (deprecated)
/*
//call this method to set main queue managedObjectContext
+ (void)setPersistentStoreManagedObjectContext:(NSManagedObjectContext *)_persistentStoreManagedObjectContext
{
    if (persistentStoreManagedObjectContext == nil) {
#if !__has_feature(objc_arc)
        persistentStoreManagedObjectContext = [_persistentStoreManagedObjectContext retain];
#else
        persistentStoreManagedObjectContext = _persistentStoreManagedObjectContext;
#endif
    }
}

+ (instancetype)sharedInstance
{
    if (sharedInstance == nil) {
        @synchronized(self) {
            if (sharedInstance == nil) sharedInstance = [[self instanceWithManagedObjectContextType:NSMainQueueConcurrencyType
                                                                                      parentContext:persistentStoreManagedObjectContext
                                                                             mergeChangesFromParent:YES] retain];
        }
    }
    
    return sharedInstance;
}
//*/

//call this method to set main queue managedObjectContext
+ (void)setPersistentStoreManagedObjectContext:(NSManagedObjectContext *)_persistentStoreManagedObjectContext
{
    if (!CDUDatabaseStack) {
        CDUDatabaseStack = [[NSMutableDictionary alloc] init];
    }
    if (![CDUDatabaseStack objectForKey:NSStringFromClass([self class])]) {
        [CDUDatabaseStack setObject:_persistentStoreManagedObjectContext forKey:NSStringFromClass([self class])];
    }
}

//desired initializer for +sharedInstance of subclasses
//to have always main context based on NSMainQueueConcurrencyType
+ (instancetype)instanceWithMainQueueConcurrencyTypeContext
{
#if !__has_feature(objc_arc)
    return [[[self alloc] initWithConcurrencyContextType:NSMainQueueConcurrencyType
                                           parentContext:[CDUDatabaseStack objectForKey:NSStringFromClass([self class])] mergeChangesFromParent:YES] autorelease];
#else
    return [[self alloc] initWithConcurrencyContextType:NSMainQueueConcurrencyType
                                          parentContext:[CDUDatabaseStack objectForKey:NSStringFromClass([self class])] mergeChangesFromParent:YES];
#endif
}

//needs to throw an exeption if sublasses attempt to call it
//means that each sublcass should implement its own +sharedIntsance
+ (instancetype)sharedInstance
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"%@ should implement its own +sharedInstance method. See example in CDUDatabase.h", NSStringFromClass([self class])] userInfo:nil];
}

- (id)init
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"%@ should +instanceWithMainQueueConcurrencyType (desired initializer) or other initializers. See example in CDUDatabase.h", NSStringFromClass([self class])] userInfo:nil];
}

//override
- (void)customSetup
{
    //do something more in sublasses, if needed
    //to add additional functionality to init
}

//main initializer
//create managed object context for instance with specified managedObjectContext's concurrency type
- (id)initWithConcurrencyContextType:(NSManagedObjectContextConcurrencyType)type
                       parentContext:(NSManagedObjectContext *)parentContext
              mergeChangesFromParent:(BOOL)mergeChangesFromParent
{
    self = [super init];
    
    if (self) {

        NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:type];
        
#if !__has_feature(objc_arc)
        self.context = [context autorelease];
#else
        self.context = context;
#endif
        
        NSManagedObjectContext *persistentStoreManagedObjectContext = [CDUDatabaseStack objectForKey:NSStringFromClass([self class])];
        NSAssert(persistentStoreManagedObjectContext, @"Sorry Bomber, I found an error on initializing object model context. It seems that your parent context is nil");
        
        __block NSManagedObjectContext *_parentContext = parentContext;
        [self.context performBlockAndWait:^{
            if (!_parentContext) {
                _parentContext = persistentStoreManagedObjectContext;
            }
            self.context.parentContext = _parentContext;
            
            if (mergeChangesFromParent && _parentContext) {
                DebugLog(@"Current context has subscribed to [\"NSManagedObjectContextDidSaveNotification\"] to receive changes from its parent");
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(contextDidSave:)
                                                             name:NSManagedObjectContextDidSaveNotification
                                                           object:_parentContext];
            }
        }];
        
        [self customSetup];
    }
    
    return self;
}

//returns autoreleased instance
+ (instancetype)instanceWithManagedObjectContextType:(NSManagedObjectContextConcurrencyType)type
{
#if !__has_feature(objc_arc)
    return [[[self alloc] initWithConcurrencyContextType:type parentContext:nil mergeChangesFromParent:NO] autorelease];
#else
    return [[self alloc] initWithConcurrencyContextType:type parentContext:nil mergeChangesFromParent:NO];
#endif
    
}

//returns autoreleased instance
+ (instancetype)instanceWithManagedObjectContextType:(NSManagedObjectContextConcurrencyType)type
                                        parentContext:(NSManagedObjectContext *)parentContext
                               mergeChangesFromParent:(BOOL)mergeChangesFromParent
{
#if !__has_feature(objc_arc)
    return [[[self alloc] initWithConcurrencyContextType:type parentContext:parentContext mergeChangesFromParent:mergeChangesFromParent] autorelease];
#else
    return [[self alloc] initWithConcurrencyContextType:type parentContext:parentContext mergeChangesFromParent:mergeChangesFromParent];
#endif
    
}

//release CDUDatabaseStack
+ (void)releaseCoreDataStack
{
    NSManagedObjectContext *persistentStoreManagedObjectContext = [CDUDatabaseStack objectForKey:NSStringFromClass([self class])];
    
    //reset context
    [persistentStoreManagedObjectContext reset];
    
    //remove context
    [CDUDatabaseStack removeObjectForKey:NSStringFromClass([self class])];
    
#if !__has_feature(objc_arc)
    if ([CDUDatabaseStack count] == 0) [CDUDatabaseStack release];
    //[sharedInstance release];
#endif
    if ([CDUDatabaseStack count] == 0) CDUDatabaseStack = nil;
    //sharedInstance = nil;
}

#pragma mark -
#pragma mark NSManagedObjectContextDidSaveNotification: merging changes from parent

//se ci sono dei problemi, studiare questa parte
- (void)contextDidSave:(NSNotification*)notification
{
    [self.context mergeChangesFromContextDidSaveNotification:notification];
}

//overrided
- (NSString *)description
{
    return [NSString stringWithFormat:@"context [%@], with concurrency type [%@]", self.context, [self formatTypeToString:self.context.concurrencyType]];
}
 
@end
