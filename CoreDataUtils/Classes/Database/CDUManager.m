//  Created by Karen Lusinyan on 6/19/13.
//  Copyright (c) 2013 Home. All rights reserved.

#import "CDUManager.h"
#import "CDUDatabase.h"
#import <objc/message.h>
#import "CDUUtil.h"

@interface CDUManager ()

@property (readwrite, nonatomic, strong) NSArray *bundles;
@property (readwrite, nonatomic, strong) NSString *databaseName;
@property (readwrite, nonatomic, strong) NSString *path;

@property (readwrite, nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (readwrite, nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (readwrite, nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;
- (NSURL *)storeURL;

@end

@implementation CDUManager

#if !__has_feature(objc_arc)
- (void) dealloc
{
    [_bundles release];
    [_databaseName release];
    [_path release];
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
    [super dealloc];
}
#endif

#pragma mark -
#pragma mark - public methods

+ (instancetype)instanceManager
{
#if !__has_feature(objc_arc)
    return [[[self alloc] init] autorelease];
#else
    return [[self alloc] init];
#endif
}

+ (instancetype)sharedManager
{
    static dispatch_once_t pred = 0;
    static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (void)configureWithBundles:(NSArray *)bundles
                databaseName:(NSString *)databaseName
                        path:(NSString *)path
                   className:(NSString *)className
{
    NSAssert(bundles, @"Bundles cannot be nil");
    self.bundles = bundles;

    NSAssert(databaseName && ([databaseName hasSuffix:@".sqlite"]), @"Sorry Bomber, please enter database name with extension .sqlite");
    self.databaseName = databaseName;
    
    self.path = path;
    
    //launch core data configuration
    NSManagedObjectContext *context = [self managedObjectContext];
    NSAssert(context, @"Sorry Bomber, i found an error on initializing object model context");
    NSAssert(NSClassFromString(className), @"Sorry Bomber, there is no class with className");
    
    //initialize database main queue context
    Class class = NSClassFromString(className);
    DebugLog(@"[class superclass] is [%@]", [class superclass]);
    NSAssert(([class isSubclassOfClass:[CDUDatabase class]]), @"Sorry Bomber, ensure that your class extends CDUDatabase");
    [class setPersistentStoreManagedObjectContext:context];
}

#pragma mark -
#pragma mark Public method to configure database

- (void)configureWithBundle:(NSBundle *)bundle
               databaseName:(NSString *)databaseName
                       path:(NSString *)path
                  className:(NSString *)className
{
    if (bundle == nil) bundle = [NSBundle mainBundle];
    [self configureWithBundles:@[bundle] databaseName:databaseName path:path className:className];
}

#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *) managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSAssert(self.bundles, @"Sorry, Bomber something went wrong: the managedObjectModel's bundle is nil");
    
#if !__has_feature(objc_arc)
    _managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:self.bundles] retain];
#else
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:self.bundles];
#endif
    
    return _managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */

- (NSPersistentStoreCoordinator *) persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self storeURL];
    
    // handle db upgrade
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        
        // Perform lightweight migration by supposing that the model versioning is already done
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
            DebugLog(@"It was not possible to recreate the DB store");
        }
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark -
#pragma mark CoreData Utility methods

- (NSURL *)storeURL
{
    return [[NSURL fileURLWithPath:self.path] URLByAppendingPathComponent:self.databaseName];
}

#pragma mark -
#pragma mark Accessors

- (NSString *)path
{
    if (_path == nil) {
#if !__has_feature(objc_arc)
        _path = [[[self applicationDocumentsDirectory] path] retain];
#else
        _path = [[self applicationDocumentsDirectory] path];
#endif
    }
    return _path;
}

@end
