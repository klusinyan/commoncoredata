//  Created by Karen Lusinyan on 6/19/13.
//  Copyright (c) 2013 Home. All rights reserved.

@interface CDUManager : NSObject

@property (readonly, nonatomic, strong) NSManagedObjectModel *managedObjectModel;

//used for multiple modules
+ (instancetype)instanceManager;

//used for single module
+ (instancetype)sharedManager;

//call this method to configure core data stack, if bundle is nil then it uses [NSBundle mainBundle]
- (void)configureWithBundle:(NSBundle *)bundle
               databaseName:(NSString *)databaseName
                       path:(NSString *)path
                  className:(NSString *)className;

@end
