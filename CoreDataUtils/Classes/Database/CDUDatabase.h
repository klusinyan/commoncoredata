//  Created by Karen Lusinyan on 9/3/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

#import <CoreData/CoreData.h>

@interface CDUDatabase : NSObject

//the context that used for fetching
@property(readonly, nonatomic, strong) NSManagedObjectContext *context;

//call this statc method to link and set main persistent store managedObjectContext
//the implicit concurrency type is NSPrivateQueueConcurrencyType by default
+ (void)setPersistentStoreManagedObjectContext:(NSManagedObjectContext *)persistentStoreManagedObjectContext;

//desired initializer for sharedInstance of subclasses
//returns shared instance of main queue type (NSMainQueueConcurrencyType)
//you use this queue type for contexts linked to controllers and UI objects that are required to be used only on the main thread
//the context is a direct child of persistentStoreManagedObjectContext and it merges automatically changes from it
//autoreleased for non ARC projects
+ (instancetype)instanceWithMainQueueConcurrencyTypeContext;

//Starting from v1.4.0 +sharedInstance should be implemented in subclasses by calling +instanceWithMainQueueConcurrencyTypeContext
//Otherwise it will throw an execption if you try to invoke it directly
+ (instancetype)sharedInstance;

/* example of usage:
+ (instancetype)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil; //(use key __strong for arch)
    dispatch_once(&pred, ^{
        //retain if non ARC enviorment (_sharedObject = [[super instanceWithMainQueueConcurrencyTypeContext] retain])
        _sharedObject = [super instanceWithMainQueueConcurrencyTypeContext];
    });
    return _sharedObject;
}
 //*/

//override
//defualt behaviour does nothing.
//should be implemented by subclasses to add aditional setup
- (void)customSetup;

//instance with specified concurrency type
//if you request the context with NSConfinementConcurrencyType,
//then you promise that context will not be used by any thread other than the one on which you created it
//Note: This context is attached to persistent store managedObjectContext and does not merge changes from its parent
+ (instancetype)instanceWithManagedObjectContextType:(NSManagedObjectContextConcurrencyType)type;

//Note: If the parent context is nil, then the current context attached to persistent store managedObjectContext
//mergeChangesFromParent: set YES to receive changes from the parent context
+ (instancetype)instanceWithManagedObjectContextType:(NSManagedObjectContextConcurrencyType)type
                                       parentContext:(NSManagedObjectContext *)parentContext
                              mergeChangesFromParent:(BOOL)mergeChangesFromParent;

//call this method to release shared instance and persistentStoreManagedObjectContext
+ (void)releaseCoreDataStack;

@end
