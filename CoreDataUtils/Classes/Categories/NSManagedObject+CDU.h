//  Created by Karen Lusinyan on 19/11/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

@interface NSManagedObject (CDU)

- (BOOL)isNew;

@end
