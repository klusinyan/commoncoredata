//  Created by Karen Lusinyan on 5/8/12.
//  Copyright (c) 2012 Home. All rights reserved.

#import "NSManagedObjectContext+CDU.h"

@implementation NSManagedObjectContext (CDU)

#pragma mark -
#pragma mark Private methods

//utility method to check integrity of entity
- (void)checkIntegrityForEntityName:(NSString *)entityName withAttributes:(NSDictionary *)attributes
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
    
    //keys to create compound predicate predicate
    NSArray *keys = [attributes allKeys];
    
    //get entity for current managedObjectContext
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self];
    
    //get all entity attributes
    NSArray *entityAttributes = [NSArray arrayWithArray:[[entity attributesByName] allKeys]];
    
    //check if attributeKeys are present in entity attributes, then proceed
    for (int i = 0; i < [keys count]; i++) {
        id attributeKey = [keys objectAtIndex:i];
        id value = [attributes objectForKey:attributeKey];
        NSAssert([entityAttributes containsObject:attributeKey], @"Sorry Bomber, it seems you passed wrong attribute key name \"%@\"",attributeKey);
        NSAssert((value && value != [NSNull null]), @"Sorry Bomber, it seems you passed nil value \"%@\" for key \"%@\"",value, attributeKey);
    }
#pragma clang diagnostic pop    
}

#pragma mark -
#pragma mark Fetch all unsorted

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                                  error:(NSError **)error
{
    return [self fetchObjectsForEntityName:entityName
                             withPredicate:nil
                           sortDescriptors:nil
                                     error:error];
}

#pragma mark -
#pragma mark Fetch all sorted

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                              sortByKey:(NSString*)key
                              ascending:(BOOL)ascending
                                error:(NSError **)error
{
    return [self fetchObjectsForEntityName:entityName
                             withPredicate:nil
                                 sortByKey:key
                                 ascending:ascending
                                     error:error];
}

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                        sortDescriptors:(NSArray*)sortDescriptors
                                  error:(NSError **)error
{
    return [self fetchObjectsForEntityName:entityName
                             withPredicate:nil
                           sortDescriptors:sortDescriptors
                                     error:error];
}

#pragma mark -
#pragma mark Fetch filtered unsorted

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                                  error:(NSError **)error
{
    return [self fetchObjectsForEntityName:entityName
                             withPredicate:predicate
                           sortDescriptors:nil
                                     error:error];
}

#pragma mark -
#pragma mark Fetch filtered sorted

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                              sortByKey:(NSString*)key
                              ascending:(BOOL)ascending
                                  error:(NSError **)error
{
    NSSortDescriptor* sort = [[NSSortDescriptor alloc] initWithKey:key
                                                         ascending:ascending];
    
#if !__has_feature(objc_arc)
    [sort autorelease];
#endif
    
    return [self fetchObjectsForEntityName:entityName
                             withPredicate:predicate
                           sortDescriptors:[NSArray arrayWithObject:sort]
                                     error:error];
}

// returnsObjectsAsFaults is NO
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                                  error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = NO;
        
    if (predicate) {
        request.predicate = predicate;
    }
    
    if (sortDescriptors) {
        request.sortDescriptors = sortDescriptors;
    }
    
    return [self executeFetchRequest:request error:error];
}

// returnsObjectsAsFaults is NO
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                               distinct:(BOOL)distinct
                      propertiesToFetch:(NSArray *)propertiesToFetch
                             limitFetch:(BOOL)limitFetch
                         withFetchLimit:(NSUInteger)fetchLimit
                                  error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = NO;
    
    if (predicate) {
        request.predicate = predicate;
    }
    
    if (sortDescriptors) {
        request.sortDescriptors = sortDescriptors;
    }

    if (distinct) {
        request.returnsDistinctResults = YES;
        request.propertiesToFetch = propertiesToFetch;
        [request setResultType:NSDictionaryResultType];
    }
    
    if (limitFetch) {
        request.fetchLimit = fetchLimit;
    }
    
    return [self executeFetchRequest:request error:error];
}

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                                groupBy:(NSArray *)groupBy
                      propertiesToFetch:(NSArray *)propertiesToFetch
                             fetchLimit:(NSInteger)fetchLimit
                                  error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = NO;
    
    if (predicate) {
        request.predicate = predicate;
    }
    
    if (sortDescriptors) {
        request.sortDescriptors = sortDescriptors;
    }

    if (propertiesToFetch != nil) {
        request.returnsDistinctResults = YES;
        request.propertiesToFetch = propertiesToFetch;
        [request setResultType:NSDictionaryResultType];
    }

    if (groupBy) {
        request.propertiesToGroupBy = groupBy;
    }
    
    if (fetchLimit > 0) {
        request.fetchLimit = fetchLimit;
    }
    
    return [self executeFetchRequest:request error:error];
}

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                      propertiesToFetch:(NSArray *)propertiesToFetch
                 returnsDistinctResults:(BOOL)returnsDistinctResults
                 returnsObjectsAsFaults:(BOOL)returnsObjectsAsFaults
                         fetchBatchSize:(NSUInteger)fetchBatchSize
                                  error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.fetchBatchSize = fetchBatchSize;
    request.returnsObjectsAsFaults = returnsObjectsAsFaults;
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif
    
    request.entity = entity;
    
    if (predicate) {
        request.predicate = predicate;
    }
    
    if (sortDescriptors) {
        request.sortDescriptors = sortDescriptors;
    }
    
    if (propertiesToFetch) {
        request.returnsDistinctResults = returnsDistinctResults;
        request.propertiesToFetch = propertiesToFetch;
        [request setResultType:NSDictionaryResultType];
    }
    
    return [self executeFetchRequest:request error:error];
}

- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                          fetchReqeuest:(NSFetchRequest *)fetchRequest
                                  error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
#if !__has_feature(objc_arc)
    NSFetchRequest *request = [fetchRequest retain];
#else
    NSFetchRequest *request = fetchRequest;
#endif
    if (!request) {
        request = [[NSFetchRequest alloc] init];
    }
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif
    
    request.entity = entity;
    
    if (predicate) {
        request.predicate = predicate;
    }
    
    if (sortDescriptors) {
        request.sortDescriptors = sortDescriptors;
    }
    
    return [self executeFetchRequest:request error:error];
}

- (NSInteger) countObjectsForEntity:(NSString *)entityName
                              error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = YES;
    request.includesPropertyValues = NO;
    
    return [self countForFetchRequest:request error:error];
}

- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                           propertiesToFetch:(NSArray *)propertiesToFetch
                                       error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = YES;
    request.propertiesToFetch = propertiesToFetch;
    
    return [self executeFetchRequest:request error:error];
}

- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                                       error:(NSError **)error
{
    return [self quickFetchObjectsForEntityName:entityName withPredicate:nil error:error];
}

- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                               withPredicate:(NSPredicate *)predicate
                                       error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];

#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = YES;
    request.includesPropertyValues = NO;

    if (predicate) {
        request.predicate = predicate;
    }
    
    return [self executeFetchRequest:request error:error];
}

- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                                  limitFetch:(BOOL)limitFetch
                                  fetchLimit:(NSInteger)fetchLimit
                                       error:(NSError **)error
{
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:self];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
#if !__has_feature(objc_arc)
    [request autorelease];
#endif

    request.entity = entity;
    request.returnsObjectsAsFaults = YES;
    request.includesPropertyValues = NO;
    
    if (limitFetch) {
        request.fetchLimit = fetchLimit;
    }

    return [self executeFetchRequest:request error:error];
}

- (NSManagedObject *)fetchObjectForEntityName:(NSString *)entityName
                             withAttributeKyes:(NSDictionary *)attributeKeys
                                         error:(NSError **)error
{
    //check entity's integrity
    [self checkIntegrityForEntityName:entityName withAttributes:attributeKeys];
    
    //construct compound attribute to fetch
    NSMutableArray *predicates = [NSMutableArray arrayWithCapacity:[attributeKeys count]];
    NSPredicate *compoundPredicate = nil;
    for (NSString *keyName in [attributeKeys allKeys]) {
        NSExpression *lhs = [NSExpression expressionForKeyPath:keyName];
        NSExpression *rhs = [NSExpression expressionForConstantValue:[attributeKeys objectForKey:keyName]];
        NSPredicate *predicateTemplate = [NSComparisonPredicate
                                          predicateWithLeftExpression:lhs
                                          rightExpression:rhs
                                          modifier:NSDirectPredicateModifier
                                          type:NSEqualToPredicateOperatorType
                                          options:NSCaseInsensitivePredicateOption];
        
        [predicates addObject:predicateTemplate];
    }
    compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    NSArray *fetchResult = [self fetchObjectsForEntityName:entityName withPredicate:compoundPredicate error:error];
    if ([fetchResult count] > 0) {
        return [fetchResult objectAtIndex:0];
    }
    
    return nil;
}

- (NSManagedObject *)createObjectForEntityName:(NSString*)entityName
{
    NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self];
    
    NSAssert(object, @"Sorry Bomber, something went wrong with your entity");
    
    return object;
}

- (NSManagedObject *)createObjectForEntityName:(NSString*)entityName withAttributes:(NSDictionary *)attributes
{
    NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self];
    
    NSAssert(object, @"Sorry Bomber, something went wrong with your entity");
    
    for (NSString *keyName in [attributes allKeys]) {
        if ([attributes objectForKey:keyName] != [NSNull null]) {
            [object setValue:[attributes objectForKey:keyName] forKey:keyName];
        }
    }
    return object;
}

- (NSManagedObject *)createOrUpdateObjectForEntityName:(NSString *)entityName
                                         withAttributes:(NSDictionary *)attributes
                                            andKeyNames:(NSArray *)keyNames
                                                  error:(NSError **)error
{
    NSMutableDictionary *attributeKeys = [NSMutableDictionary dictionaryWithCapacity:[keyNames count]];
    for (NSString *keyName in keyNames) {
        if ([attributes objectForKey:keyName] != [NSNull null]) {
            [attributeKeys setObject:[attributes objectForKey:keyName] forKey:keyName];
        }
    }
    
    NSManagedObject *object = [self fetchObjectForEntityName:entityName withAttributeKyes:attributeKeys error:error];
    if (object == nil) {
        object = [self createObjectForEntityName:entityName withAttributes:attributes];
    }
    else {
        for (NSString *keyName in [attributes allKeys]) {
            if ([attributes objectForKey:keyName] != [NSNull null]) {
                [object setValue:[attributes objectForKey:keyName] forKey:keyName];
            }
        }
    }
    
    return object;
}

- (void)deleteObjectsForEntityName:(NSString *)entityName withPredicate:(NSPredicate *)predicate error:(NSError **)error
{
    NSArray *fetchResult = [self quickFetchObjectsForEntityName:entityName withPredicate:predicate error:error];
    for (int i = 0; i < [fetchResult count]; i++) {
        if ([fetchResult objectAtIndex:i] != nil) {
            [self deleteObject:[fetchResult objectAtIndex:i]];
        }
    }
}

- (BOOL)saveToPersistentStore:(NSError **)error
{
    __block NSError *localError = nil;
    NSManagedObjectContext *contextToSave = self;
    while (contextToSave) {
        __block BOOL success;
        
        /**
         To work around issues in ios 5 first obtain permanent object ids for any inserted objects.  If we don't do this then its easy to get an `NSObjectInaccessibleException`.  This happens when:
         
         1. Create new object on main context and save it.
         2. At this point you may or may not call obtainPermanentIDsForObjects for the object, it doesn't matter
         3. Update the object in a private child context.
         4. Save the child context to the parent context (the main one) which will work,
         5. Save the main context - a NSObjectInaccessibleException will occur and Core Data will either crash your app or lock it up (a semaphore is not correctly released on the first error so the next fetch request will block forever.
         */
        [contextToSave obtainPermanentIDsForObjects:[[contextToSave insertedObjects] allObjects] error:&localError];
        if (localError) {
            if (error) *error = localError;
            return NO;
        }
        
        [contextToSave performBlockAndWait:^{
            success = [contextToSave save:&localError];
            if (!success && localError == nil) DebugLog(@"Saving of managed object context failed, but a `nil` value for the `error` argument was returned. This typically indicates an invalid implementation of a key-value validation method exists within your model. This violation of the API contract may result in the save operation being mis-interpretted by callers that rely on the availability of the error.");
        }];
        
        if (!success) {
            if (error) *error = localError;
            return NO;
        }
        
        if (!contextToSave.parentContext && contextToSave.persistentStoreCoordinator == nil) {
            DebugLog(@"Reached the end of the chain of nested managed object contexts without encountering a persistent store coordinator. Objects are not fully persisted.");
            return NO;
        }
        contextToSave = contextToSave.parentContext;
    }
    
    return YES;
}

@end