//  Created by Karen Lusinyan on 5/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
#pragma mark version - 1.1.0

@interface NSManagedObjectContext (CDU)

#pragma mark -
#pragma mark Fetch all unsorted

/** @brief Convenience method to fetch all objects for a given Entity name in
 * this context.
 *
 * The objects are returned in the order specified by Core Data.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                                  error:(NSError **)error;

#pragma mark -
#pragma mark Fetch all sorted

/** @brief Convenience method to fetch all objects for a given Entity name in
 * the context.
 *
 * The objects are returned in the order specified by the provided key and
 * order.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                              sortByKey:(NSString*)key
                              ascending:(BOOL)ascending
                                  error:(NSError **)error;

/** @brief Convenience method to fetch all objects for a given Entity name in
 * the context.
 *
 * If the sort descriptors array is not nil, the objects are returned in the
 * order specified by the sort descriptors. Otherwise, the objects are returned
 * in the order specified by Core Data.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                        sortDescriptors:(NSArray*)sortDescriptors
                                  error:(NSError **)error;

#pragma mark -
#pragma mark Fetch filtered unsorted

/** @brief Convenience method to fetch selected objects for a given Entity name
 * in the context.
 *
 * If the predicate is not nil, the selection is filtered by the provided
 * predicate.
 *
 * The objects are returned in the order specified by Core Data.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                                  error:(NSError **)error;

#pragma mark -
#pragma mark Fetch filtered sorted

/** @brief Convenience method to fetch selected objects for a given Entity name
 * in the context.
 *
 * If the predicate is not nil, the selection is filtered by the provided
 * predicate.
 *
 * The objects are returned in the order specified by the provided key and
 * order.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                              sortByKey:(NSString*)key
                              ascending:(BOOL)ascending
                                  error:(NSError **)error;

/** @brief Convenience method to fetch selected objects for a given Entity name
 * in the context.
 *
 * If the predicate is not nil, the selection is filtered by the provided
 * predicate.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                                  error:(NSError **)error;

#pragma mark -
#pragma mark Custom fetch

/** @brief Convenience method to fetch selected objects DISTINCT for a given Entity name
 * in the context.
 *
 * If the predicate is not nil, the selection is filtered by the provided
 * predicate.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                               distinct:(BOOL)distinct
                      propertiesToFetch:(NSArray *)propertiesToFetch
                             limitFetch:(BOOL)limitFetch
                         withFetchLimit:(NSUInteger)fetchLimit
                                  error:(NSError **)error __deprecated;

/** @brief Convenience method to fetch selected objects GROUPBY for a given Entity name
 * in the context.
 * returnsObjectsAsFaults is NO
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                                groupBy:(NSArray *)groupBy
                      propertiesToFetch:(NSArray *)propertiesToFetch
                             fetchLimit:(NSInteger)fetchLimit
                                  error:(NSError **)error __deprecated;

/** @brief Convenience method to fetch desired properties and return array of dictionary 
 * in the context.
 * returnsObjectsAsFaults shoud be set, otherwise uses default YES
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                      propertiesToFetch:(NSArray *)propertiesToFetch
                 returnsDistinctResults:(BOOL)returnsDistinctResults
                 returnsObjectsAsFaults:(BOOL)returnsObjectsAsFaults
                         fetchBatchSize:(NSUInteger)fetchBatchSize
                                  error:(NSError **)error;

#pragma mark -
#pragma mark Personalized fetch

/** @brief Convenience method to personalize the fetch by passing NSFetchRequest object
 * Note: if fetchReqeuest is nil, it fateches by using defualy request default properties
 * in the context.
 */
- (NSArray *)fetchObjectsForEntityName:(NSString*)entityName
                          withPredicate:(NSPredicate*)predicate
                        sortDescriptors:(NSArray*)sortDescriptors
                          fetchReqeuest:(NSFetchRequest *)fetchRequest
                                  error:(NSError **)error;

/** @brief Convenience method to quick fetch selected objects DISTINCT for a given Entity name
 * in the context.
 * returnsObjectsAsFaults is YES
 */
- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                           propertiesToFetch:(NSArray *)propertiesToFetch
                                       error:(NSError **)error;

/** @brief Convenience method to quick fetch selected objects
 * in the context.
 * returnsObjectsAsFaults is YES
 * includesPropertyValues is NO
 */
- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                                       error:(NSError **)error;

/** @brief Convenience method to quick fetch selected objects
 * in the context with given predicate.
 * returnsObjectsAsFaults is YES
 * includesPropertyValues is NO
 */
- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                               withPredicate:(NSPredicate *)predicate
                                       error:(NSError **)error;

/** @brief Convenience method to quick fetch selected objects
 * in the context with fetchLimit.
 * returnsObjectsAsFaults is YES
 * includesPropertyValues is NO
 */
- (NSArray *)quickFetchObjectsForEntityName:(NSString*)entityName
                                  limitFetch:(BOOL)limitFetch
                                  fetchLimit:(NSInteger)fetchLimit
                                       error:(NSError **)error;


/** @brief Convenience method to quick fetch selected objects count
 * in the context.
 * returnsObjectsAsFaults is YES
 * includesPropertyValues is NO
 */
- (NSInteger)countObjectsForEntity:(NSString *)entityName
                              error:(NSError **)error;


/** @brief Convenience method to fetch an object:
 if attributeKeys is nil returns the first row
 */
- (NSManagedObject *)fetchObjectForEntityName:(NSString *)entityName
                             withAttributeKyes:(NSDictionary *)attributeKeys
                                         error:(NSError **)error;


/** @brief Convenience method to create an object:
 returns a new object for given entity
 */
- (NSManagedObject *)createObjectForEntityName:(NSString*)entityName;

/** @brief Convenience method to create an object:
 returns a new object for given entity with attributeKeys
 */
- (NSManagedObject *)createObjectForEntityName:(NSString*)entityName
                                 withAttributes:(NSDictionary *)attributes;


/** @brief Convenience method to fetch or create an object:
 withAttributes: all entity attributes which will beeing updated
 keyNames: array of key names included in dicitionary attributes
 returns an object object with identifier or creates if not exists
 */
- (NSManagedObject *)createOrUpdateObjectForEntityName:(NSString *)entityName
                                         withAttributes:(NSDictionary *)attributes
                                            andKeyNames:(NSArray *)keyNames
                                                  error:(NSError **)error;


/** @brief Convenience method to delete all objects
    for given entity
 */
- (void)deleteObjectsForEntityName:(NSString *)entityName
                      withPredicate:(NSPredicate *)predicate
                              error:(NSError **)error;

/**
 Saves the receiver and then traverses up the parent context chain until a parent managed object context with a nil parent is found. If the final ancestor context does not have a reference to the persistent store coordinator, then a warning is generated and the method returns NO.
 
 @param error If there is a problem saving the receiver or any of its ancestor contexts, upon return contains an pointer to an instance of NSError that describes the problem.
 @return YES if the save to the persistent store was successful, else NO.
 */
// duplicato da analoga categoria in ResKit
- (BOOL)saveToPersistentStore:(NSError **)error;

@end