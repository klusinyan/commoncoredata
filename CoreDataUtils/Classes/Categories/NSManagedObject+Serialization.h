//  Created by Karen Lusinyan on 16/10/14.
//  Copyright (c) 2014 Karen Lusinyan. All rights reserved.

@interface NSManagedObject (Serialization)

- (NSDictionary *)toDictionary;

- (void)populateFromDictionary:(NSDictionary*)dict;

+ (NSManagedObject *)createManagedObjectFromDictionary:(NSDictionary*)dict
                                             inContext:(NSManagedObjectContext*)context;

@end
