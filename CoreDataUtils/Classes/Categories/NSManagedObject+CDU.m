//  Created by Karen Lusinyan on 19/11/13.
//  Copyright (c) 2013 Karen Lusinyan. All rights reserved.

#import "NSManagedObject+CDU.h"

@implementation NSManagedObject (CDU)

- (BOOL)isNew
{
    NSDictionary *vals = [self committedValuesForKeys:nil];
    return [vals count] == 0;
}

@end
