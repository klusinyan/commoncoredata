Pod::Spec.new do |s|
  s.name         = 'CoreDataUtils'
  s.version      = '1.5.2'
  s.summary      = 'CoreData Utilities.'
  s.homepage     = 'https://bitbucket.org/mrklteam/coredatautils'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'Karen Lusinyan' => 'karen.lusinyan.developerios@gmail.com' }
  s.platform     = :ios, '7.0'
  s.source       = { :git => 'https://bitbucket.org/mrklteam/coredatautils.git', :tag => s.version.to_s }

  s.prefix_header_file = 'CoreDataUtils/Classes/Lib-Prefix.pch'

  s.requires_arc = false

  s.subspec 'Constants' do |ss|
    ss.source_files = 'CoreDataUtils/Classes/Constants/*.h'
  end

  s.subspec 'Categories' do |ss|
    ss.source_files = 'CoreDataUtils/Classes/Categories/*.{h,m}'
  end

  s.subspec 'Utils' do |ss|
    ss.source_files = 'CoreDataUtils/Classes/Utils/*.{h,m}'
    ss.dependency 'CoreDataUtils/Constants'
  end

  s.subspec 'Database' do |ss|
    ss.source_files = 'CoreDataUtils/Classes/Database/*.{h,m}'
    ss.dependency 'CoreDataUtils/Utils'
    ss.dependency 'CoreDataUtils/Categories'
  end

end